# ESP32-CAN
## Overview
![image](Images/overview.jpg)

ESP32-CAN is a custom dev board for wireless CAN bus sniffing.  
Current design ultilizes a DB9 port. By using a DB9 to OBD2 cable, it can also sniff data on a automobile's CAN networl.

## Current Status

Schematic - Done  
PCB - WIP  
Firmware - WIP

